# java-jdbc-mysql

#### 介绍
纯 Java 通过 JDBC 直连 MySQL 数据库

#### 使用说明

1、笔者的本地数据库版本为 MySQL 8.0.16，数据库账号及密码为：root/12345678，如果你本地数据库的账号密码不一样，请在项目的包 com.gorge4j 下的 JavaJdbcMysqlDemo.java 类里修改成自己的账号密码，注意密码需 base64 加密 [去加密](http://tool.oschina.net/encrypt?type=3)；  
2、在 MySQL 中创建数据库，数据库名：jdbc-mysql-demo，字符集：utf8mb4，排序规则：utf8mb4\_bin；  
3、在 MySQL 中创建表，SQL 如下：

```sql
CREATE TABLE `user_demo` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id，用户id',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '用户名',
  `password` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '用户密码',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='用户示例表';
```

4、数据库表中初始化一条管理员数据  

```sql
INSERT INTO `jdbc-mysql-demo`.`user_demo`(`id`, `name`, `password`) VALUES (1, 'admin', '123456');
```

5、将项目下载到本地，然后通过 IDE（Eclipse/IDEA） 导入项目后直接启动运行项目查看效果。