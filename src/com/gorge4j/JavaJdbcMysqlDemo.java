package com.gorge4j;

import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Base64;
import java.util.logging.Logger;

/**
 * @Title: JavaJdbcMysqlDemo.java
 * @Description: 纯 Java 通过 JDBC 直连 MySQL 数据库
 * @Copyright: © 2019 XXX
 * @Company: XXXXXX
 *
 * @author gorge.guo
 * @date 2019-04-01 20:46:52
 * @version v1.0
 */

public class JavaJdbcMysqlDemo {
	
	/** 引入JDK自带日志打印的对象，替换堆栈打印的方式 */
	static Logger log = Logger.getLogger("JavaJdbcMysqlDemo");
	
	/** 声明数据库驱动类的路径，低版本的jdbc驱动注意选择这个包：com.mysql.jdbc.Driver */
	static final String DRIVER = "com.mysql.jdbc.Driver";
	/** 声明数据库连接URL */
	static final String DB_URL = "jdbc:mysql://localhost:3306/jdbc-mysql-demo?useSSL=false";
	/** 声明数据库帐号 */
	static final String DB_USER = "root";
	/** 声明数据库密码，避免SonarLint的告警，对密码进行了加密，原文：12345678 */
	static final String DB_PASS = "MTIzNDU2Nzg=";

	public static void main(String[] args) {
		/** 定义数据库连接对象 */
		Connection conn = null;
		/** 定义一个可编译和执行 SQL 的对象，用于将 SQL 语句发送到数据库中 */
		Statement stmt = null;
		/** 定义数据库结果集对象 */
		ResultSet rs = null;
		try {
			// 通过反射的特性加载数据库驱动，使用高版本驱动时请对应导入高版本的驱动包，例如本次导入的是 mysql-connector-java-8.0.15.jar 包
			Class.forName(DRIVER);
			// 获取数据库连接对象
			conn = DriverManager.getConnection(DB_URL, DB_USER, aesDecrypt(DB_PASS));
			// 在给定连接上执行 SQL 语句
			stmt = conn.createStatement();
			// 定义 SQL 语句
			String strSql = "SELECT id, name, password FROM user_demo";
			// 执行 SQL，并将查询结果赋值给结果集对象
			rs = stmt.executeQuery(strSql);
			// 打印数据输出的标题
			log.info("ID | NAME | PASSWORD ");
			// 判断结果集并输出数据
			while (rs.next()) {
				// 获取数据
				int id = rs.getInt("id");
				String name = rs.getString("name");
				String password = rs.getString("password");
				// 组装结果
				String strRs = id + " | " + name + " | " + password;
				// 输出数据
				log.info(strRs);
			}
			// 关闭结果集
			rs.close();
			// 关闭表连接对象
			stmt.close();
			// 关闭数据库连接对象
			conn.close();
		} catch (SQLException se) {
			// 处理数据库相关异常
			log.warning(se.getMessage());
		} catch (Exception e) {
			// 处理其它异常
			log.warning(e.getMessage());
		} finally {
			// 关闭结果集资源
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException se) {
				log.warning(se.getMessage());
			}
			// 关闭表连接资源
			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException se) {
				log.warning(se.getMessage());
			}
			// 关闭数据库连接资源
			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException se) {
				log.warning(se.getMessage());
			}
		}
	}
	
	/**
	 * Base64解密
	 * @param encode 需解密的字符串
	 * @return 解密后的结果
	 */
	private static String aesDecrypt(String encode) {
	    try {
	        byte[] by = Base64.getDecoder().decode(encode.getBytes(StandardCharsets.UTF_8));
	        return new String(by, StandardCharsets.UTF_8);
	    } catch (Exception e) {
	    	log.warning(e.getMessage());
	    }
	    return null;
	}
	
	/**
	 * Base64加密
	 * @param decode 需加密的字符串
	 * @return 加密后的结果
	 */
	public static String aesEncrypt(String decode) {
	    try {
	        return Base64.getEncoder().encodeToString(decode.getBytes(StandardCharsets.UTF_8));
	    } catch (Exception e) {
	    	log.warning(e.getMessage());
	    }
	    return null;
	}


}